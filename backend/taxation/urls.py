from django.conf.urls import include, url
from rest_framework import routers

from .views import StateViewset, SettingTaxPeriodViewset, SettingTaxDueDateViewset, ZipCodeViewset, FileUploadViewset

file_uploads = FileUploadViewset.as_view({
    'put': 'put'
})

file_list = FileUploadViewset.as_view({
    'get': 'list'
})


api_router = routers.SimpleRouter()
api_router.register(r'states', StateViewset, basename='state')
api_router.register(r'periods', SettingTaxPeriodViewset, basename='periods')
api_router.register(r'duedates', SettingTaxDueDateViewset, basename='duedates')
api_router.register(r'zipcodes', ZipCodeViewset, basename='zipcodes')
# api_router.register(r'uploaders/(?P<filename>[^/]+)$', file_uploads, basename='file_uploaders')
# api_router.register(r'uploaders', file_list, basename='file_list')


urlpatterns = [
    url(r'', include(api_router.urls)),
    url(r'uploader', file_list, name='file_list'),
    url(r'uploader', file_uploads, name='file_uploaders')
    # url(r'uploaders/(?P<filename>[^/]+)$', file_uploads, name='file_uploaders')
]
