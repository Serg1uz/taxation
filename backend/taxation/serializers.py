from rest_framework_json_api import serializers

from .models import State, SettingTaxPeriod, SettingTaxDueDate, ZipCode \
    , FileUpload


class SettingTaxPeriodSerializer(serializers.ModelSerializer):
    """
        Serializing Tax Period Settings
    """

    class Meta:
        model = SettingTaxPeriod
        fields = '__all__'


class SettingTaxDueDatedSerializer(serializers.ModelSerializer):
    """
            Serializing Tax Due Date Settings
    """

    class Meta:
        model = SettingTaxDueDate
        fields = '__all__'


class StateSerializer(serializers.ModelSerializer):
    """
            Serializing State
    """
    tax_period = SettingTaxPeriodSerializer
    tax_due_date = SettingTaxDueDatedSerializer

    class Meta:
        model = State
        fields = '__all__'


class ZipCodeSerializer(serializers.ModelSerializer):
    """
            Serializing Zip Codes
    """
    state = StateSerializer

    class Meta:
        model = ZipCode
        fields = '__all__'


class FileUploadSerializer(serializers.ModelSerializer):
    """
            Serializing File Upload
    """

    class Meta:
        model = FileUpload
        fields = '__all__'
