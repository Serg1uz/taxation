from django.db import models


class SettingTaxPeriod(models.Model):
    name = models.CharField(max_length=255, help_text='Enter Name', verbose_name='Name', unique=True)
    description = models.TextField(verbose_name='Description', blank=True, null=True)

    class Meta:
        ordering = ['name']


class SettingTaxDueDate(models.Model):
    name = models.CharField(max_length=255, help_text='Enter Name', verbose_name='Name', unique=True)
    description = models.TextField(verbose_name='Description', blank=True, null=True)

    class Meta:
        ordering = ['name']


class State(models.Model):
    name = models.CharField(max_length=255, verbose_name='State Name')
    link = models.CharField(max_length=255, verbose_name='State Link', blank=True,
                            null=True)
    abbr = models.CharField(max_length=4, verbose_name='State Abbreviation')
    is_enabled = models.BooleanField(verbose_name='Enable Taxation', auto_created=False, default=False)
    date_enabled = models.DateField(verbose_name='Enabled Date', blank=True, null=True)
    include_shipping = models.BooleanField(verbose_name='Include Shipping Cost', blank=True, null=True)
    include_handling_fee = models.BooleanField(verbose_name='Include Handling Fee', blank=True, null=True)
    is_hard = models.BooleanField(verbose_name='Hard State', blank=True, null=True)
    min_tax_rate = models.DecimalField(verbose_name='Min Tax Rate', max_digits=6, decimal_places=5, blank=True,
                                       null=True)
    max_tax_rate = models.DecimalField(verbose_name='Max Tax Rate', max_digits=6, decimal_places=5, blank=True,
                                       null=True)
    fixed_tax_rate = models.DecimalField(verbose_name='Fixed Tax Rate', max_digits=6, decimal_places=5, blank=True,
                                         null=True)
    # include_difference_shipping = models.BooleanField(verbose_name='Include difference Shipping', blank=True,
    # null=True)
    tax_period = models.OneToOneField(SettingTaxPeriod, verbose_name="Sales Tax Period", on_delete=models.RESTRICT)
    tax_due_date = models.OneToOneField(SettingTaxDueDate, verbose_name="Sales Tax Due Date", on_delete=models.RESTRICT)
    created_on = models.DateTimeField(verbose_name='Created Date', auto_now_add=True)
    updated_on = models.DateTimeField(verbose_name='Updated Date', auto_now_add=True)

    class Meta:
        ordering = ['name']


class ZipCode(models.Model):
    zip5 = models.CharField(max_length=5, verbose_name='Zip5')
    zip4 = models.CharField(max_length=4, verbose_name='Zip4', blank=True, null=True)
    state = models.OneToOneField(State, verbose_name='Sate', on_delete=models.RESTRICT)
    county = models.CharField(max_length=255, verbose_name='County Name')
    tax_rate = models.DecimalField(verbose_name='Max Tax Rate', max_digits=6, decimal_places=5, default=0)

    class Meta:
        ordering = ['state', 'zip5']

class FileUpload(models.Model):
    TYPE_DATA = [
        ('tp', 'Tax Period'),
        ('td', 'Tax Due Date'),
        ('st', 'State'),
        ('zc', 'Zip Code'),
        ('nn', 'None'),
    ]

    STATUS_UPLOAD = [
        ('rc', 'Receive'),
        ('vd', 'Validate'),
        ('sc', 'Success'),
        ('dc', 'Decline'),
    ]

    file_name = models.CharField(max_length=255, verbose_name='File Name')
    path = models.CharField(max_length=255, verbose_name='Path ')
    type = models.CharField(max_length=2, choices=TYPE_DATA, default='nn', verbose_name='Type Data')
    status = models.CharField(max_length=2, choices=STATUS_UPLOAD, default='rc', verbose_name='Status')
    owner = models.CharField(max_length=255, verbose_name='File Owner')
    created_on = models.DateTimeField(verbose_name='Created Date', auto_now_add=True)
