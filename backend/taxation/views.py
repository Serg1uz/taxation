from rest_framework_csv.parsers import CSVParser
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.parsers import JSONParser, MultiPartParser

from .models import State, SettingTaxPeriod, SettingTaxDueDate, ZipCode, FileUpload
from .serializers import StateSerializer, SettingTaxPeriodSerializer, SettingTaxDueDatedSerializer, ZipCodeSerializer, \
    FileUploadSerializer


class BaseViewSet(viewsets.ModelViewSet):
    def get_queryset(self):
        return self.model.objects.all()


class SettingTaxPeriodViewset(BaseViewSet):
    serializer_class = SettingTaxPeriodSerializer
    model = SettingTaxPeriod


class SettingTaxDueDateViewset(BaseViewSet):
    serializer_class = SettingTaxDueDatedSerializer
    model = SettingTaxDueDate


class StateViewset(BaseViewSet):
    serializer_class = StateSerializer
    model = State


class ZipCodeViewset(BaseViewSet):
    serializer_class = ZipCodeSerializer
    model = ZipCode


class FileUploadViewset(BaseViewSet):
    serializer_class = FileUploadSerializer
    model = FileUpload
    parser_classes = (JSONParser, MultiPartParser, CSVParser)

    # @action(methods=['get'], detail=True)
    # def list(self, request, *args, **kwargs):
    #     # queryset = FileUpload.objects.all()
    #     # serializer = self.serializer_clases(self.serializer_clases, many=True)
    #     return Response(self.s)

    # @action(detail=False, methods=['put'], name='Uploader View', parser_classes=[CSVParser, JSONParser], )
    # def uploader(self, request, filename, format=None):
    #     # Parsed data will be returned within the request object by accessing 'data' attr
    #     _data = request.data
    #     print(_data)
    #     print(filename)
    #
    #     return Response(status=204)
    @action(methods=['put'], detail=False, name='Uploader View', parser_classes=[CSVParser, JSONParser], )
    def put(self, request):
        print('put it work')
        type_upload = request.query_params.get('type')
        owner = request.query_params.get('owner')
        if type_upload is None or owner is None:
            return Response(status='400', data='bad request')

        _data = request.data

        # print(request.data)
        return Response(status=204)
