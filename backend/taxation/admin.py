from django.contrib import admin

from .models import SettingTaxPeriod, SettingTaxDueDate, State, ZipCode \
    , FileUpload

admin.site.register(SettingTaxPeriod)
admin.site.register(SettingTaxDueDate)
admin.site.register(State)
admin.site.register(ZipCode)
admin.site.register(FileUpload)
